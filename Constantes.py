import pygame
from pygame import mixer

def sound_winner():
    winner_sound = pygame.mixer.Sound("winner.wav")
    pygame.mixer.Sound.play(winner_sound)

NOIR = (0, 0, 0)
BLANC = (255, 255, 255)
CORAIL = pygame.Color(255, 90, 90)
GRIS = pygame.Color(64, 64, 64)
TUILE_TAILLE = 30
TUILE_NOMBRE = 24
SCORE_HAUTEUR = 32
INVENTAIRE_LARGEUR = 64
LARGEUR = TUILE_TAILLE * TUILE_NOMBRE + INVENTAIRE_LARGEUR
HAUTEUR = TUILE_TAILLE * TUILE_NOMBRE + SCORE_HAUTEUR
MONSTRE_GAUCHE = False
MONSTRE_DROITE = True
SHOOTER = False
SHOOTER_RUNNING = False
MINIJEUX = [False, False, False]
# On définit les polices du jeu
pygame.init()
police = pygame.font.Font('Police1.ttf', 20)
police2 = pygame.font.Font('Police2.ttf', 15)

# On crée la variable fenetre pour l'écran
fenetre = pygame.display.set_mode((LARGEUR, HAUTEUR))
