import pygame

from Constantes import *


def sound_wingames():
    wingames_sound = pygame.mixer.Sound("wingames.wav")
    pygame.mixer.Sound.play(wingames_sound)

# On crée trois listes de sprites pour les tuyaux, les sprites de fond
# #et la liste d'objectifs (sprites sur lesquels il faut placer les tuyaux manquants)
LISTE_SPRITES_EVIER = pygame.sprite.Group()
LISTE_TUYAUX_EVIER = pygame.sprite.Group()
LISTE_OBJ = pygame.sprite.Group()


# On définit la classe Fond, qui est un sprite simple sans action
class FOND(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("fond.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x


# On définit la classe objectif
class OBJECTIF(pygame.sprite.Sprite):
    # Dans l'initialisation on prend le nom des sprites (en minuscule)
    def __init__(self, x, y, nom):
        pygame.sprite.Sprite.__init__(self)
        # l'image est la même que le fond
        self.image = pygame.image.load("fond.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        # L'attribut "correspondant" désigne le nom du tuyau qu'il faut placer. Il s'agit de la majuscule du
        # paramètre "nom"
        self.correspondant = nom.upper()
        self.rempli = False

    def update(self):
        # On détecte si le sprite d'objectif est en collision avec un tuyau.
        COLLISION_TUYEAU = pygame.sprite.spritecollide(self, LISTE_TUYAUX_EVIER, False)
        for tuyau in COLLISION_TUYEAU:
            # Pour chaque tuyau de cette liste de collision on regarde si son nom correspond à l'attribut "correspondant"
            if (tuyau.nom == self.correspondant):
                # Si oui, on regarde si ce tuyau a bien été laché
                if tuyau.tenu == False:
                    # Si c'est le cas on superpose le rectangle du tuyau à celui de l'objectif
                    tuyau.rect.x = self.rect.x
                    tuyau.rect.y = self.rect.y
                    # On change l'attribut "movable" pour que le tuyau ne soit pas rempli
                    tuyau.movable = False
                    # On change l'attribut rempli de l'objectif
                    self.rempli = True


# On définit la classe tuyau
class TUYAU(pygame.sprite.Sprite):
    # Dans l'initialisation on rajoute le paramètre "nom" qui correspond au caractère dans le fichier Tuyeau.txt
    def __init__(self, x, y, nom):
        pygame.sprite.Sprite.__init__(self)
        # On sauvegarde le nom dans l'attribut "nom"
        self.nom = nom
        self.image = pygame.image.load(nom + ".png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        # Par défaut les tuyaux ne peuvent pas être bougés (movable est False)
        self.movable = False
        # Par défaut, les tuyaux ne sont pas tenus
        self.tenu = False

    def update(self):
        # Si le tuyau est bougeable et est tenu alors la position du rectabgle est celle de la souris

        if self.movable & self.tenu:
            self.rect.center = pygame.mouse.get_pos()


def evier_reparation():
    global LISTE_TUYAUX_EVIER
    from Classes import attente_message
    # On initialise le paramètre d'arrêt de la boucle de notre jeu
    EVIER_RUNNING = True
    # On initialise les coordonnées à 0
    XX = 0
    YY = 0
    # On ouvre le fichier Tuyaux en lecture
    with open("Tuyaux", "r") as fichier:
        # Pour chaque ligne du fichier
        for ligne in fichier:
            # Pour chaque caractère de chaque ligne
            for sprite in ligne:
                if (sprite == "."):
                    # Si le sprite est un point on crée une instance fond et on l'ajoute à la liste des sprites
                    fond = FOND(XX, YY)
                    LISTE_SPRITES_EVIER.add(fond)
                elif (sprite.isupper()):
                    # Si le sprite est une majuscule c'est un tuyau, on ajoute un sprite fond et un sprite tuyaux à
                    # ces coordonées et on les ajoute aux listes correspondantes
                    fond = FOND(XX, YY)
                    LISTE_SPRITES_EVIER.add(fond)
                    tuyau = TUYAU(XX, YY, sprite)
                    LISTE_TUYAUX_EVIER.add(tuyau)
                    # Si les tuyaux sont dans le 21ème caractère de la ligne alors leur attribut movable devient
                    # vrai (ce sont les tuyaux à déplacer)
                    if (XX == 20):
                        tuyau.movable = True
                elif (sprite.islower()):
                    # Si le sprite est une minuscule, alors on crée une instance objectif et on l'ajoute à la liste
                    # des objectifs
                    objectif = OBJECTIF(XX, YY, sprite)
                    LISTE_OBJ.add(objectif)

                # On incrémente XX pour passer au caractère suivant
                XX += 1
            # On revient au caractère 0 de la ligne
            XX = 0
            # On incremente la ligne7
            YY += 1

        # Le jeu continue tant que EVIER_RUNNING est TRUE
        while EVIER_RUNNING:
            # On crée une horloge pour le jeu
            clock = pygame.time.Clock()
            # On fait en sorte que le programme ne fasse pas plus de 30 frames par secondes (on est pas sur un AAA)
            clock.tick(30)

            # La fenetre est rempli de noir (pour cacher les sprites du jeu général et les mouvements de sprites)
            fenetre.fill(NOIR)
            # On dessine les sprites de l'évier et des tuyaux
            LISTE_SPRITES_EVIER.draw(fenetre)
            LISTE_TUYAUX_EVIER.draw(fenetre)

            # On attends les evenements
            for event in pygame.event.get():
                # Si l'evenement quitter est detecté on quitte le jeu
                if event.type == pygame.QUIT:
                    LISTE_TUYAUX_EVIER = False
                    break
                # Lorsque le bouton de la souris est appuyé
                if event.type == pygame.MOUSEBUTTONDOWN:
                    for tuyau in LISTE_TUYAUX_EVIER:
                        # Si le rectangle d'un tuyau est en collision avec la position de la souris alors le tuyau
                        # est tenu
                        if tuyau.rect.collidepoint(event.pos):
                            tuyau.tenu = True
                # Lorsque le bouton de la souris est relâché
                elif event.type == pygame.MOUSEBUTTONUP:
                    # Si le rectangle d'un tuyau est en collision avec la position de la souris alors le tuyau
                    # n'est plus tenu
                    for tuyau in LISTE_TUYAUX_EVIER:
                        tuyau.tenu = False
            # On initialise la variable d'objectifs remplis
            nb_obj_rempli = 0
            for objectif in LISTE_OBJ:
                # Pour chaque objectif de la liste d'objectif, s'ils sont remplis (objectif.rempli = True) on incrémente
                if objectif.rempli:
                    nb_obj_rempli += 1
            #Si le nb d'objectif rempli est égal à la longueur de la liste d'objectifs
            if nb_obj_rempli == len(LISTE_OBJ):
                #On met à jour et dessine les tuyaux (pour avoir leur dernière position)
                LISTE_TUYAUX_EVIER.update()
                LISTE_TUYAUX_EVIER.draw(fenetre)
                #On génère et affiche le message de réussite
                sound_wingames()
                texte = police2.render("BRAVO", False, BLANC)
                fenetre.blit(texte, (HAUTEUR / 2, LARGEUR / 2))
                pygame.display.update()
                #On appelle la fonction de message d'attente
                attente_message()
                #On arrête la boucle
                EVIER_RUNNING = False
                #on inscrit que le mini jeu est fini dans la liste des mini-jeux
                MINIJEUX[2] = True

                # Si une touche clavier est activée, on regarde si elle fait partie des commandes du jeu

            # On actualise et dessine les différents
            LISTE_TUYAUX_EVIER.update()
            LISTE_OBJ.update()
            LISTE_OBJ.draw(fenetre)
            LISTE_TUYAUX_EVIER.draw(fenetre)
            pygame.display.update()
    #On efface la liste des sprites si l'on veut recommencer le mini jeu
    LISTE_SPRITES_EVIER.empty()
    LISTE_TUYAUX_EVIER.empty()
    LISTE_OBJ.empty()
