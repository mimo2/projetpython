import pygame, Shooter

from Constantes import *
from Classes import *
from pygame import mixer
import Jeu_evier

def sound_get():
    get_sound = pygame.mixer.Sound("Itemget.wav")
    pygame.mixer.Sound.play(get_sound)


def sound_select():
    select_sound = pygame.mixer.Sound("Select.wav")
    pygame.mixer.Sound.play(select_sound)


def sound_pose():
    poser_sound = pygame.mixer.Sound("Poseobjet.wav")
    pygame.mixer.Sound.play(poser_sound)


# Bonjour
def menu():
    global afficher, RUNNING
    for evnmt in pygame.event.get():
        if evnmt.type == pygame.QUIT:
            RUNNING = False
    pygame.draw.rect(fenetre, CORAIL, ((0, 0), (HAUTEUR, LARGEUR)))  # On dessine la fenetre du menu
    # On dessine le rectangle qui sert de bouton pour le menu lancer
    lancer = pygame.draw.rect(fenetre, CORAIL, ((190, 150), (150, 30)))
    # On dessine le rectangle qui sert de bouton pour le menu quitter
    quitter = pygame.draw.rect(fenetre, CORAIL, ((190, 200), (150, 30)))
    # On créer les trois variables de texte
    Titre = police.render('Escape Lockdown ', False, GRIS)
    Lancer = police.render('Lancer le jeu ', False, GRIS)
    Quitter = police.render('Quitter ', False, GRIS)
    # On affiche les trois textes
    fenetre.blit(Titre, (200, 50))
    fenetre.blit(Lancer, (200, 150))
    fenetre.blit(Quitter, (200, 200))
    # On crée une variable souris à laquelle on affecte les booléens associés aux différents boutons de la souris
    souris = pygame.mouse.get_pressed()
    # Si le clic droit de la souris est activé
    if souris[0]:
        # On recupère la position de la souris
        souris_pos = pygame.mouse.get_pos()
        # On teste si la souris touche le rectangle de lancer
        if lancer.collidepoint(souris_pos):
            # Si c'est le cas on affecte la fonction jeu à la variable afficher (on quitte le menu)
            sound_select()
            afficher = jeu
            # afficher = jeu
        elif quitter.collidepoint(souris_pos):
            # On redessine un rectangle pour masquer le texte precedent
            sound_select()
            pygame.draw.rect(fenetre, CORAIL, ((0, 0), (HAUTEUR, LARGEUR)))
            # On crée une variable pour le texte d'au revoir
            Quitter = police.render('A bientôt ! ', False, GRIS)
            # On affiche cette variable
            fenetre.blit(Quitter, (200, 200))
            # On actualise l'affichage
            pygame.display.update()
            # On attends 3 secondes pour laisser le temps de lire le texte
            pygame.time.wait(3000)
            # On change le statut du booléen RUNNING pour mettre fin à la boucle principale et donc au jeu
            RUNNING = False


def jeu():
    global RUNNING

    # On crée deux variables pour les coordonnées
    XX = 0
    YY = 0
    # On ouvre un fichier Texte pour créer la salle
    with open("Salle1.txt", "r") as fichier:
        # Pour chaque ligne du fichier
        for ligne in fichier:
            # Pour chaque caractère de chaque ligne
            for sprite in ligne:

                if (sprite != ".") and (sprite != '\n'):
                    # On créé une nouvelle dalle de sol que l'on ajoute aux groupes sprite
                    sol = SOL(XX, YY)  # Les coordonnées sont le n° de caractère et le n° de ligne
                    LISTE_SPRITES.add(sol)
                # Si le caractère est un M
                if sprite == "M":
                    # On créé un nouveau mur que l'on ajoute aux groupes sprite et mur
                    mur = MUR(XX, YY)  # Les coordonnées sont le n° de caractère et le n° de ligne
                    LISTE_SPRITES.add(mur)
                # Si le caractère est un T
                elif sprite == "T":
                    # On créé une nouvelle table que l'on ajoute aux groupes sprite et mur
                    table = TABLE(XX, YY)
                    LISTE_SPRITES.add(table)
                elif sprite == "L":
                    lit = LIT(XX, YY)
                    LISTE_SPRITES.add(lit)
                elif sprite == "O":
                    oreiller = OREILLER(XX, YY)
                    LISTE_SPRITES.add(oreiller)
                elif sprite == "R":
                    TABLE(XX, YY)
                    RADIO(XX, YY)
                elif sprite == "E":
                    EVIER(XX, YY)
                elif sprite == "Z":
                    DECHET(XX, YY)
                elif sprite == "U":
                    POUBELLE(XX, YY)
                elif sprite == "P":
                    PORTE(XX, YY)
                elif sprite == "X":
                    perso = PERSONNAGE(XX, YY)
                    LISTE_SPRITES_ANIMES.add(perso)
                    selecteur = SELECTEUR(XX, YY)
                    LISTE_SPRITES_PARAM.add(selecteur)
                elif sprite == "W":
                    wc = WC(XX, YY)
                    LISTE_SPRITES.add(wc)
                elif sprite == "C":
                    chaise = CHAISE(XX, YY)
                    LISTE_SPRITES.add(chaise)
                elif sprite == "B":
                    bain = BAIN(XX, YY)
                    LISTE_SPRITES.add(bain)
                elif sprite == "F":
                    cheminee = CHEMINEE(XX, YY)
                    LISTE_SPRITES.add(cheminee)
                elif sprite == "V":
                    tele = TELE(XX, YY)
                    LISTE_SPRITES.add(tele)
                elif sprite == "G":
                    console = CONSOLE(XX, YY)
                    LISTE_SPRITES.add(console)
                elif sprite == "J":
                    fleur = FLEUR(XX, YY)
                    LISTE_SPRITES.add(fleur)
                elif sprite == "Y":
                    baignoire = BAIGNOIRE(XX, YY)
                    LISTE_SPRITES.add(baignoire)
                elif sprite == "1":
                    tapis = TAPIS(XX, YY)
                    LISTE_SPRITES.add(tapis)
                elif sprite == "2":
                    frigo = FRIGO(XX, YY)
                    LISTE_SPRITES.add(frigo)
                # On incrémente XX pour passer au caractère suivant
                XX = XX + 1
            # On revient au caractère 0 de la ligne
            XX = 0
            # On incremente la ligne
            YY = YY + 1
    inventaire = INVENTAIRE()
    LISTE_SPRITES.add(inventaire)
    selecteur_inv = SELECTEUR_INV()
    LISTE_SPRITES_SELECT_INV.add(selecteur_inv)

    # On met à jour les sprites
    LISTE_SPRITES.update()
    # On rajoute de la musique
    mixer.init()
    mixer.music.load('Musique1.mp3')
    mixer.music.play(-1)

    # Le jeu continue tant que RUNNING est TRUE
    while RUNNING:
        # On fait en sorte que le programme ne fasse pas plus de 30 frames par secondes (on est pas sur un AAA)
        clock.tick(30)

        # On remplit la fenetre de noir (pour le fond)
        fenetre.fill(NOIR)
        # On dessine les sprites
        LISTE_SPRITES.draw(fenetre)
        LISTE_SPRITES_SELECT_INV.draw(fenetre)
        # On attends les evenements
        COLLISION_ACTION = pygame.sprite.spritecollide(selecteur, LISTE_SPRITES_ACTION, False)
        COLLISION_INVENTAIRE = pygame.sprite.spritecollide(selecteur, LISTE_SPRITES_INVENTAIRE, False)
        if len(COLLISION_ACTION) > 0:
            texte = police2.render(selecteur.action, False, BLANC)
            fenetre.blit(texte, (300, 000))

        for event in pygame.event.get():
            # Si l'evenement quitter est detecté on quitte le jeu
            if event.type == pygame.QUIT:
                RUNNING = False
                break
            # Si une touche clavier est activée, on regarde si elle fait partie des commandes du jeu
            elif event.type == pygame.KEYDOWN:
                # On modifie la direction chez le personnage pour qu'il bouge
                if event.key == pygame.K_LEFT:
                    perso.DIRECTION[0] = 'L'
                    break
                if event.key == pygame.K_RIGHT:
                    perso.DIRECTION[0] = 'R'
                    break
                if event.key == pygame.K_UP:
                    perso.DIRECTION[0] = 'U'
                    break
                if event.key == pygame.K_DOWN:
                    perso.DIRECTION[0] = 'D'
                    break
                if event.key == pygame.K_SPACE:
                    for sprite in COLLISION_ACTION:
                        sprite.ACTION = not (sprite.ACTION)
                    for sprite in COLLISION_INVENTAIRE:
                        inventaire.ajout_objet(sprite)
                        sound_get()
                        sprite.kill()
                if event.key == pygame.K_a:
                    inventaire.enlever_objet(selecteur)
                    sound_pose()
                if event.key == pygame.K_TAB:

                    if inventaire.objet_actif+1 < len(inventaire.contenu):
                        inventaire.objet_actif +=1
                    else:
                        inventaire.objet_actif = 0




        # On actualise et dessine les sprites animés
        LISTE_SPRITES_ANIMES.update()
        LISTE_SPRITES_ACTION.update()

        LISTE_SPRITES_SELECT_INV.update(inventaire)
        LISTE_SPRITES_ACTION.draw(fenetre)
        LISTE_SPRITES_INVENTAIRE.draw(fenetre)
        LISTE_SPRITES_ANIMES.draw(fenetre)


        pygame.display.update()


# On crée la boucle principale
def boucle_principale():
    global RUNNING
    while RUNNING:
        fenetre.fill((0, 0, 0))
        # On fait en sorte que le programme ne fasse pas plus de 30 frames par secondes (on est pas sur un AAA)
        clock.tick(30)

        ## Exécute la fonction affecté à afficher (menu/jeu)
        afficher()
        pygame.display.update()

    pygame.quit()


# On initialise pygame
pygame.init()

# On met le titre sur la fenetre Pygame qui s'ouvrira
pygame.display.set_caption("Escape Lockdown")

# On initialise RUNNING en True
RUNNING = True

# On crée une horloge pour le jeu
clock = pygame.time.Clock()

# On affecte la fonction menu à la variable afficher
afficher = menu

# On lance la fonction boucle principale
boucle_principale()
