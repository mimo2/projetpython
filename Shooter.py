import pygame

from Constantes import *
from Classes import *
from pygame import mixer

def sound_monstershooted():
    monstershooted_sound = pygame.mixer.Sound("monstershooted.wav")
    pygame.mixer.Sound.play(monstershooted_sound)

def sound_wingames():
    wingames_sound = pygame.mixer.Sound("wingames.wav")
    pygame.mixer.Sound.play(wingames_sound)

def sound_gameover():
    gameover_sound = pygame.mixer.Sound("gameover.wav")
    pygame.mixer.Sound.play(gameover_sound)

def sound_fireball():
    fireball_sound = pygame.mixer.Sound("fireball.wav")
    pygame.mixer.Sound.play(fireball_sound)

# On définit la classe monstre
class MONSTRE(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.transform.scale(pygame.image.load("monstre.png").convert_alpha(),
                                            (TUILE_TAILLE, TUILE_TAILLE))
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        # On ajoute un attribut direction pour déterminer vers où le monstre se déplace
        self.direction = ""

    def update(self):

        if (self.rect.x <= 0):
            # Si le monstre dépasse la gauche de l'écran, on lui dit de repartir à droite (direction = D)
            self.direction = "D"
            # On descend le monstre de 3 cases
            self.rect.y += TUILE_TAILLE * 3
        elif self.rect.x >= LARGEUR - 2 * TUILE_TAILLE:
            # Si le monstre dépasse la droite de l'écran, on lui dit de repartir à gauche (direction = G)
            self.direction = "G"
            # On descend le monstre de 3 cases
            self.rect.y += TUILE_TAILLE * 3
        if self.direction == "D":
            # Si la direction est droite alors le monstre bouge d'une demi case
            self.rect.x += TUILE_TAILLE / 2
        else:
            # Si la direction est gauche alors le monstre bouge d'une demi case
            self.rect.x -= TUILE_TAILLE / 2
            # Si le monstre dépasse la ligne du personnage (Y = 22 * Tuile_Taille)
        if self.rect.y > 22 * TUILE_TAILLE:
            # On réassigne la ligne du personnage (Y = 22 * Tuile_Taille) au monstre
            self.rect.y = 22 * TUILE_TAILLE

        # On crée une liste de collision avec la liste de personnages
        COLLISION_MONSTRE = pygame.sprite.spritecollide(self, LISTE_SPRITES_PERSO, False)
        for perso in COLLISION_MONSTRE:
            # Tous les personnages dans cette liste sont tués et on remplit la fenetre de noir
            perso.kill()
            LISTE_SPRITES_PERSO.remove(perso)
            fenetre.fill(NOIR)


# On crée la classe Feu pour les boules de feux du shooter
class FEU(pygame.sprite.Sprite):
    def __init__(self, x, y):

        pygame.sprite.Sprite.__init__(self)
        self.images = []  # on fait une liste images à laquelle on ajoute des images
        # On anime la boule de feu pour qu'elle soit plus jolie
        self.images.append(pygame.transform.scale(pygame.image.load("fireball1.png").convert_alpha(),
                                                  (TUILE_TAILLE, TUILE_TAILLE)))  # image 1 de la liste
        self.images.append(pygame.transform.scale(pygame.image.load("fireball2.png").convert_alpha(),
                                                  (TUILE_TAILLE, TUILE_TAILLE)))  # image 2 de la liste
        self.images.append(pygame.transform.scale(pygame.image.load("fireball3.png").convert_alpha(),
                                                  (TUILE_TAILLE, TUILE_TAILLE)))  # image 3 de la liste
        self.images.append(pygame.transform.scale(pygame.image.load("fireball4.png").convert_alpha(),
                                                  (TUILE_TAILLE, TUILE_TAILLE)))  # image 4 de la liste
        self.id_frame = 0  # on initialise la frame à 0
        self.image = self.images[self.id_frame]  # l'image de base est la frame 0
        self.rect = self.image.get_rect()
        self.rect.y = y
        self.rect.x = x

    def update(self):
        # A chaque update la boule de feu avance vers le haut d'une demi case
        self.rect.y -= TUILE_TAILLE / 2
        # On crée une liste pour vérifier si la boule de feu rentre en collision avec des sprites
        # de la liste monstre
        COLLISION_FEU = pygame.sprite.spritecollide(self, LISTE_SPRITES_MONSTRE, False)
        for objet in COLLISION_FEU:
            #Pour chaque objet dans cette liste on supprime l'objet et la boule de feu
            sound_monstershooted()
            objet.kill()
            self.kill()
        if self.rect.y < 0:
            #Si la boule de feu dépasse l'écran on la supprime
            self.kill()

        self.id_frame += 0.1  # on incrémente la frame de 0.1 (pour que les frames n'aillent pas trop vite,
        # on est sur 15 fps)
        if math.floor(self.id_frame) >= len(self.images):
            self.id_frame = 0  # si la partie entière de l'id de la frame est plus grand que le total de frames on
            # repart à 0
        self.image = self.images[math.floor(self.id_frame)]  # on actualise l'image à chaque update


def minishooter():
    import Constantes
    SHOOTER_RUNNING = True
    Constantes.SHOOTER = True

    # On crée deux variables pour les coordonnées
    XX = 0
    YY = 0
    # On ouvre un fichier Texte pour créer la salle
    with open("Shooter.txt", "r") as fichier:
        # Pour chaque ligne du fichier
        for ligne in fichier:
            # Pour chaque caractère de chaque ligne
            for sprite in ligne:
                if (sprite != ".") and (sprite != '\n'):
                    # On créé une nouvelle dalle de sol que l'on ajoute aux groupes sprite
                    sol = SOL(XX, YY)  # Les coordonnées sont le n° de caractère et le n° de ligne
                    LISTE_SPRITES_SHOOTER.add(sol)
                if sprite == "X":
                    # Si le sprite est X on crée le personnage et on l'ajoute à la liste des sprites animes,
                    # du shooter et du personnage
                    perso_shooter = PERSONNAGE(XX, YY)
                    LISTE_SPRITES_ANIMES_SHOOTER.add(perso_shooter)  # Pour que le perso soit animé
                    LISTE_SPRITES_SHOOTER.add(perso_shooter)
                    LISTE_SPRITES_PERSO.add(perso_shooter)
                elif sprite == "O":
                    # Si le sprite est O on crée un monstre et on l'ajoute à la liste des sprites animes et la liste
                    # des monstres
                    monstre = MONSTRE(XX, YY)
                    LISTE_SPRITES_ANIMES_SHOOTER.add(monstre)
                    LISTE_SPRITES_MONSTRE.add(monstre)
                # On incrémente XX pour passer au caractère suivant
                XX = XX + 1
            # On revient au caractère 0 de la ligne
            XX = 0
            # On incremente la ligne
            YY = YY + 1

    # On met à jour les sprites

    LISTE_SPRITES_SHOOTER.update()
    # On rajoute de la musique
    mixer.init()
    mixer.music.load('shooter.mp3')
    mixer.music.play(-1)
    # On remplit la fenetre de noir (pour le fond)
    fenetre.fill(NOIR)
    # On dessine les sprites
    LISTE_SPRITES_SHOOTER.draw(fenetre)
    # Le jeu continue tant que RUNNING est TRUE
    while SHOOTER_RUNNING:
        # On crée une horloge pour le jeu
        clock = pygame.time.Clock()
        # On fait en sorte que le programme ne fasse pas plus de 15 frames par secondes (pour donner un effet retro au shooter)
        clock.tick(15)

        # On remplit la fenetre de noir et on dessine la liste des sprites
        fenetre.fill(NOIR)
        LISTE_SPRITES_SHOOTER.draw(fenetre)

        # On attends les évènements

        for event in pygame.event.get():
            # Si l'evenement quitter est detecté on quitte la boucle
            if event.type == pygame.QUIT:
                SHOOTER_RUNNING = False
                break
            # Si une touche clavier est activée, on regarde si elle fait partie des commandes du jeu
            elif event.type == pygame.KEYDOWN:
                # On modifie la direction chez le personnage pour qu'il bouge à gauche ou a droite
                if event.key == pygame.K_LEFT:
                    perso_shooter.DIRECTION[0] = 'L'
                if event.key == pygame.K_RIGHT:
                    perso_shooter.DIRECTION[0] = 'R'
                # Si on appuie sur espace alors le personnage doit tirer une boule de feu
                if event.key == pygame.K_SPACE:
                    # On crée une boule de feu, elle prend la position du personnage
                    sound_fireball()
                    boule_feu = FEU(perso_shooter.rect.x, perso_shooter.rect.y)
                    # On l'ajoute à la liste des sprites animes
                    LISTE_SPRITES_ANIMES_SHOOTER.add(boule_feu)
                    # Si on appuie sur Echap alors la musique d'arrete et on arrête le shooter
                if event.key == pygame.K_ESCAPE:
                    mixer.music.load('Musique1.mp3')
                    mixer.music.play(-1)
                    SHOOTER_RUNNING = False

        # On actualise et dessine les sprites animés
        LISTE_SPRITES_ANIMES_SHOOTER.update()
        LISTE_SPRITES_ANIMES_SHOOTER.draw(fenetre)
        pygame.display.update()

        # Si la longueur de la liste de monstres est 0 (tous les monstres sont morts)
        if len(LISTE_SPRITES_MONSTRE) == 0:
            # On arrête la boucle du shooter
            SHOOTER_RUNNING = False
            fenetre.fill(NOIR)
            mixer.music.stop()
            sound_wingames()
            # On génère et affiche le message de félicitations
            texte = police2.render("BRAVO", False, BLANC)
            fenetre.blit(texte, (HAUTEUR / 2, LARGEUR / 2))
            pygame.display.update()
            # On attend que le joueur appuie sur espace
            attente_message()
            # On signale que le mini-jeu a été gagné dans la liste des constantes
            MINIJEUX[0] = True
        # Si la liste des sprites personnages est de 0 alors le personnage a été tué
        if len(LISTE_SPRITES_PERSO) == 0:
            # On arrête la boucle du shooter
            SHOOTER_RUNNING = False
            fenetre.fill(NOIR)
            mixer.music.stop()
            sound_gameover()
            # On génère et affiche le message de fin du jeu
            texte = police2.render("GAME OVER", False, BLANC)
            fenetre.blit(texte, (HAUTEUR / 2, LARGEUR / 2))
            pygame.display.update()
            # On attend que le joueur appuie sur espace
            attente_message()

    # On vide les listes de sprites pour rejouer au mini jeu
    LISTE_SPRITES_SHOOTER.empty()
    LISTE_SPRITES_MONSTRE.empty()
    LISTE_SPRITES_PERSO.empty()
    LISTE_SPRITES_ANIMES_SHOOTER.empty()
    # On dit que le Shooter s'arrête dans les contantes
    mixer.music.load('Musique1.mp3')
    mixer.music.play(-1)
    Constantes.SHOOTER = False
