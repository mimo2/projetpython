import pygame, math, random
import Constantes
from Constantes import *
import Jeu_evier
from pygame import mixer

def sound_wingames():
    wingames_sound = pygame.mixer.Sound("wingames.wav")
    pygame.mixer.Sound.play(wingames_sound)

# On crée les differents groupes de sprites

LISTE_MURS = pygame.sprite.Group()
LISTE_SPRITES = pygame.sprite.Group()
LISTE_SPRITES_ANIMES = pygame.sprite.Group()
LISTE_SPRITES_ACTION = pygame.sprite.Group()
LISTE_SPRITES_PARAM = pygame.sprite.Group()
LISTE_SPRITES_INVENTAIRE = pygame.sprite.Group()
LISTE_DECHETS = pygame.sprite.Group()
LISTE_SPRITES_SELECT_INV = pygame.sprite.Group()
LISTE_SPRITES_SHOOTER = pygame.sprite.Group()
LISTE_SPRITES_ANIMES_SHOOTER = pygame.sprite.Group()
LISTE_SPRITES_MONSTRE = pygame.sprite.Group()
LISTE_SPRITES_PERSO = pygame.sprite.Group()


### Les Sprites sans fonction "update"###
# Ce sont les sprites qu'il suffit d'initialiser, leur fonctionnement est similaire

# On crée la classe mur
class MUR(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        # On importe l'image du mur
        self.image = pygame.image.load("Mur.png").convert_alpha()
        # On obtient le rectangle associé au mur en obtenant le rectangle de l'image
        self.rect = self.image.get_rect()
        # On positionne le rectangle en fonction des parametres initialisés (les coordonnées)
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = True
        self.anime = False
        self.action = False
        self.inventaire = False
        # Pour chaque classe on l'ajoute aux listes spécifiques en fonction de la valeur
        # de ses attributs action, anime, inventaire et collision
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)


class TABLE(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("Table.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = True
        self.anime = False
        self.action = False
        self.inventaire = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)


class CHAISE(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("Chaise.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = True
        self.anime = False
        self.action = False
        self.inventaire = True
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)


class BAIN(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("Bain.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = True
        self.anime = False
        self.action = False
        self.inventaire = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)


class LIT(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("Lit.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = True
        self.anime = False
        self.action = False
        self.inventaire = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)


class TELE(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("tv_sprite.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = True
        self.anime = False
        self.action = False
        self.inventaire = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)


class FLEUR(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("flower.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = True
        self.anime = False
        self.action = False
        self.inventaire = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)


class WC(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("WC.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = True
        self.anime = False
        self.action = False
        self.inventaire = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)


class SOL(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("Sol.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = False
        self.anime = False
        self.action = False
        self.inventaire = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)


class TAPIS(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("tapis.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = False
        self.anime = False
        self.action = False
        self.inventaire = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)


class OREILLER(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("Oreiller.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = True
        self.anime = False
        self.action = False
        self.inventaire = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)

class FRIGO(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("frigo.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = True
        self.anime = True
        self.action = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)

class BAIGNOIRE(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("Baignoire.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = True
        self.anime = False
        self.action = False
        self.inventaire = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)


### Les Sprites spécifiques ###
# Ce sont les classes les plus importantes car elles nous servent à intéragir (personnage, inventaire, sélecteur, etc.).
# On y définit plusieurs fonctions différentes
class PERSONNAGE(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        # On crée un dictionnaire python avec les 4 images par orienation
        self.images = {'R0': pygame.transform.scale(pygame.image.load("PersoR0.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'R1': pygame.transform.scale(pygame.image.load("PersoR1.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'R2': pygame.transform.scale(pygame.image.load("PersoR2.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'R3': pygame.transform.scale(pygame.image.load("PersoR3.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'L0': pygame.transform.scale(pygame.image.load("PersoL0.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'L1': pygame.transform.scale(pygame.image.load("PersoL1.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'L2': pygame.transform.scale(pygame.image.load("PersoL2.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'L3': pygame.transform.scale(pygame.image.load("PersoL3.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'U0': pygame.transform.scale(pygame.image.load("PersoU0.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'U1': pygame.transform.scale(pygame.image.load("PersoU1.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'U2': pygame.transform.scale(pygame.image.load("PersoU2.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'U3': pygame.transform.scale(pygame.image.load("PersoU3.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'D0': pygame.transform.scale(pygame.image.load("PersoD0.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'D1': pygame.transform.scale(pygame.image.load("PersoD1.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'D2': pygame.transform.scale(pygame.image.load("PersoD2.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)),
                       'D3': pygame.transform.scale(pygame.image.load("PersoD3.png").convert_alpha(),
                                                    (TUILE_TAILLE, TUILE_TAILLE)), }
        # La première image est R0
        self.image = self.images['R0']
        # On initialise le numéro de l'image à 0
        self.id_frame = 0
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        # On crée une liste DIRECTION
        self.DIRECTION = []
        # DIRECTION[0] prend la valeur "-" (pas de mouvement)
        self.DIRECTION.append('-')
        # DIRECION[1] prend la valeur "R" (orientation vers la droite)
        self.DIRECTION.append('R')

    # On crée une fonction update pour déplacer le personnage et pour qu'il ne puisse pas traverser les murs
    def update(self):
        # On obtient la position actuelle du personnage
        X_COURANT = self.rect.x
        Y_COURANT = self.rect.y

        # Si la direction[0] est égale à L / R / U ou D on change la coordonnée correspondante
        # et on remet la direction à
        # "-" (pour que le personnage ne se déplace pas à l'infini"
        if self.DIRECTION[0] == 'L':

            self.rect.x -= TUILE_TAILLE
            self.DIRECTION[0] = '-'
            # On change la direction[1] pour que le personnage soit orienté du bon côté
            self.DIRECTION[1] = 'L'
        elif self.DIRECTION[0] == 'R':
            self.rect.x += TUILE_TAILLE
            self.DIRECTION[0] = '-'
            # On change la direction[1] pour que le personnage soit orienté du bon côté
            self.DIRECTION[1] = 'R'
        elif self.DIRECTION[0] == 'U':
            self.rect.y -= TUILE_TAILLE
            self.DIRECTION[0] = '-'
            # On change la direction[1] pour que le personnage soit orienté du bon côté
            self.DIRECTION[1] = 'U'
        elif self.DIRECTION[0] == 'D':
            self.rect.y += TUILE_TAILLE
            self.DIRECTION[0] = '-'
            # On change la direction[1] pour que le personnage soit orienté du bon côté
            self.DIRECTION[1] = 'D'

        # Dans le shooter, le personnage doit bouger sans prendre en compte les murs de la liste (qui ne sont pas
        # affichés mais gardés dans la liste)
        if not Constantes.SHOOTER:
            # On crée une liste collision, elle contient tous les sprites du groupe LISTE_MURS en collision avec le
            # personnage
            COLLISION = pygame.sprite.spritecollide(self, LISTE_MURS, False)
            # Si la liste contient au moins un sprite, alors il y a collision, on ne change pas la position du personnage
            if len(COLLISION) > 0:
                self.rect.x = X_COURANT
                self.rect.y = Y_COURANT
            LISTE_SPRITES_PARAM.update(self)

        self.id_frame += 0.1  # on incrémente la frame de 0.1 (pour que les frames n'aillent pas trop vite,
        # on est sur 30 fps)
        if math.floor(self.id_frame) >= 3:
            self.id_frame = 0  # si la partie entière de l'id de la frame est plus grand que le total de frames on
            # repart à 0
        self.image = self.images[self.DIRECTION[1] + str(math.floor(self.id_frame))]


class SELECTEUR(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.transform.scale(pygame.image.load("Selecteur.png").convert_alpha(), (30, 30))
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        # On définit le message qui s'affiche lorsque le selecteur rentre en action
        self.action = ("Appuyez sur la touche Espace pour intéragir")

    def update(self, perso):
        # On obtient la position actuelle du sélecteur
        Y_COURANT = self.rect.y
        X_COURANT = self.rect.x

        # Le selecteur change en fonction de l'orienation du personnage
        if perso.DIRECTION[1] == 'L':
            self.rect.y = perso.rect.y
            self.rect.x = perso.rect.x - TUILE_TAILLE
            # Si le personnage est tourné vers la gauche alors le selecteur est sur la même ligne que lui mais une
            # colonne de moins
        elif perso.DIRECTION[1] == 'R':
            self.rect.y = perso.rect.y
            self.rect.x = perso.rect.x + TUILE_TAILLE
            # Si le personnage est tourné vers la droite alors le selecteur est sur la même ligne que lui mais une
            # colonne de plus
        elif perso.DIRECTION[1] == 'U':
            self.rect.x = perso.rect.x
            self.rect.y = perso.rect.y - TUILE_TAILLE
            # Si le personnage est tourné vers le haut alors le selecteur est sur la même colonneque lui mais une
            # ligne de moins
        elif perso.DIRECTION[1] == 'D':
            self.rect.x = perso.rect.x
            self.rect.y = perso.rect.y + TUILE_TAILLE
            # Si le personnage est tourné vers le bas alors le selecteur est sur la même colonneque lui mais une
            # ligne de plus

        # Si le personnage rentre en collision, alors le sélecteur ne bouge pas
        # On crée une liste collision, elle contient tous les sprites du groupe LISTE_MURS en collision avec le
        # personnage
        COLLISION = pygame.sprite.spritecollide(perso, LISTE_MURS, False)

        # Si la liste contient au moins un sprite, alors il y a collision, on ne change pas la position du selecteur
        if len(COLLISION) > 0:
            self.rect.x = X_COURANT
            self.rect.y = Y_COURANT


class INVENTAIRE(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("Inventaire.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = 0
        self.rect.x = 0
        # On définit l'attribut contenu qui est une liste d'objets
        self.contenu = []
        # On définit l'objet actif à 0
        self.objet_actif = 0

    # On définit la fonction d'ajout d'objet à l'inventaire
    def ajout_objet(self, objet):
        # Si l'objet est dans la liste des objets ajoutables à l'inventaire et que celui-ci n'est pas plein
        if (objet in LISTE_SPRITES_INVENTAIRE) & len(self.contenu) < 7:
            # On récupère la classe de l'objet
            classe = type(objet)
            # On obtient la coordonnée de la tuile disponible dans l'inventaire
            XX = len(self.contenu)
            # On crée l'objet dans l'inventaire
            objet_inv = classe(XX, -1)
            # On retire l'objet de l'inventaire de la liste des sprites animes (pour ne pas avoir des objets qui
            # bougent dans l'inventaire)
            LISTE_SPRITES_ANIMES.remove(objet_inv)
            # On diminue la taile de l'image de l'objet dans l'inventaire
            objet_inv.image = pygame.transform.scale(objet.image, (20, 20))
            # On ajoute l'objet crée dans la liste de contenu de l'inventaire
            self.contenu.append(objet_inv)
            # On ajoute ce nouvel objet à la liste des sprites
            LISTE_SPRITES.add(objet_inv)
            # On détruit l'objet initial
            objet.kill()

    def enlever_objet(self, selecteur):
        # Si l'inventaire n'est pas vide on enlève l'objet
        if self.contenu != []:
            # On crée un objet temporaire à partir de la liste "contenu" (objet_actif sert d'index)
            objet_temp = self.contenu[self.objet_actif]
            # On récupère la classe de l'objet temporaire
            classe = type(objet_temp)
            # On récupère les coordonnées du sélecteur afin de créer l'objet
            tuile_x = (selecteur.rect.x / TUILE_TAILLE)
            tuile_y = (selecteur.rect.y - SCORE_HAUTEUR) / TUILE_TAILLE
            # On crée le nouvel objet avec les coordonées précedemment obtenues
            objet = classe(tuile_x, tuile_y)
            # On crée une liste de collision pour voir si l'objet est en collision avec un mur
            COLLISION = pygame.sprite.spritecollide(objet, LISTE_MURS, False)
            # Tous les objets prenables dans l'inventaire sont infranchissables, donc il y aura forcément un sprite
            # dans la liste de collision
            if len(COLLISION) == 1:
                # On retire l'objet temporaire de la liste "contenu"
                self.contenu.remove(objet_temp)
                # On détruit l'objet temporaire
                objet_temp.kill()
            else:

                for objet_1 in COLLISION:
                    # Si l'objet temporaire est un dechet et que le type de l'autre objet dans la collision est une
                    # poubelle alors on enlève notre objet de l'inventaire et on lance la fonction dechet de la
                    # poubelle
                    if (type(objet_temp) == DECHET) & (type(objet_1) == POUBELLE):
                        objet_temp.kill()
                        self.contenu.remove(objet_temp)
                        objet_1.dechet()
                    # Sinon on détruit l'objet temporaire et l'objet qu'on vient de placer mais on ne touche pas au
                    # contenu de l'inventaire
                    objet_temp.kill()
                    objet.kill()
        # On actualise la position de chaque objet en fonction de son index (pour éviter les "trous" dans l'inventaire)
        for objet_pos in self.contenu:
            objet_pos.rect.x = (self.contenu.index(objet_pos)) * TUILE_TAILLE
        # Si en supprimant un objet, l'objet actif est égale à la longueur du contenu alors on diminue la valeur de l'objet actif
        if self.objet_actif == len(self.contenu):
            self.objet_actif = len(self.contenu) - 1


# Le selecteur d'inventaire est un simple carré jaune qui change en fonction de l'objet actif de l'inventaire
class SELECTEUR_INV(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("Selecteur_Inventaire.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = 0
        self.rect.x = 0

    def update(self, inventaire):
        # La case du sélecteur change en fonction de l'attribut objet_actif de l'inventaire
        self.rect.x = inventaire.objet_actif * TUILE_TAILLE


###Les sprites de mini jeu####
# Ce sont les sprites qui lancent des actions de mini-jeux
class CONSOLE(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("console.png").convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        # Cet attribut permet de savoir si l'objet a été actionné (ACTION=True) ou pas
        self.ACTION = False
        # Cette liste contient les différentes actions
        self.actions = []
        # On ajoute le texte des 4 actions
        self.actions.append("On va jouer un peu à la console...")
        self.actions.append("Droite pour aller à droite, gauche pour aller à gauche")
        self.actions.append("Espace pour tirer")
        self.actions.append("Et echap pour s'arrêter")
        # Cet attribut est le compteur d'action
        self.id_action = 0
        #
        self.action_actuelle = self.actions[self.id_action]
        self.collision = True
        self.anime = True
        self.action = True
        self.inventaire = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)

    def update(self):
        # Lorsque l'objet est actionné
        if self.ACTION == True:
            # Tant que l'identifiant est plus petit que le compteur d'actions
            while self.id_action < len(self.actions):
                # On change l'action_actuelle
                self.action_actuelle = self.actions[self.id_action]
                # On incrémente le compteur
                self.id_action += 1
                # On affiche le message de l'action et on attent la confirmation du joueur
                fenetre.fill(NOIR)
                texte = police2.render(self.action_actuelle, False, BLANC)
                fenetre.blit(texte, (25, 25))
                pygame.display.update()
                attente_message()
            # Une fois finit on réinitialise le compteur d'action
            self.id_action = 0
            # Une fois toutes les actions finies on lance la fonction minishooter du fichier Shooter.py
            import Shooter
            Shooter.minishooter()
            # On désactive l'attribut ACTION
            self.ACTION = False


class DECHET(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        # On crée une liste d'image et on y ajoute 3 images
        self.images = []  # on fait une liste images à laquelle on ajoute des images
        self.images.append(pygame.transform.scale(pygame.image.load("Poubelle1.PNG").convert_alpha(),
                                                  (TUILE_TAILLE, TUILE_TAILLE)))  # image 1 de la liste
        self.images.append(pygame.transform.scale(pygame.image.load("Poubelle2.PNG").convert_alpha(),
                                                  (TUILE_TAILLE, TUILE_TAILLE)))  # image 2 de la liste
        self.images.append(pygame.transform.scale(pygame.image.load("Poubelle3.PNG").convert_alpha(),
                                                  (TUILE_TAILLE, TUILE_TAILLE)))  # image 3 de la liste
        # On choisit un entier aléatoire pour choisir l'image
        self.image = self.images[random.randint(0, 2)]
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = True
        self.anime = False
        self.action = False
        self.inventaire = True
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)
        # On ajoute le déchet à la liste
        LISTE_DECHETS.add(self)


# On crée la classe poubelle pour le mini jeu
class POUBELLE(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.transform.scale(pygame.image.load("Poubelle.PNG").convert_alpha(),
                                            (TUILE_TAILLE, TUILE_TAILLE))
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.ACTION = False
        # L'attribut déchet total est le nombre de dechets de la liste (il est important d'initialiser la poubelle
        # après les déchets !)
        self.dechet_total = len(LISTE_DECHETS)
        # On initialise le compteur de dechets collectés
        self.dechet_collect = 0
        self.collision = True
        self.anime = True
        self.action = False
        self.inventaire = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)

    def dechet(self):
        # Cette fonction est appelée lorsqu'un déchet est placé à la poubelle
        # On incrémente le compteur
        self.dechet_collect += 1
        # Lorsque le compteur est égal au nombre de déchets total on affiche le message de réussite
        if self.dechet_collect == self.dechet_total:
            fenetre.fill(NOIR)
            sound_wingames()
            texte = police2.render("Super ! Une bonne chose de faite", False, BLANC)
            fenetre.blit(texte, (25, 25))
            pygame.display.update()
            attente_message()
            # On remplit la liste de constante pour dire que le mini jeu a été réussit
            MINIJEUX[1] = True


#### Les sprites actionnables ou animés ####
# Ces sprites ne sont pas en soi des minijeux mais ils sont animés  et/ou intéractifs

# La radio sert a donner quelques informations sur le jeu, elle est aussi animée
class RADIO(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.images = []  # on fait une liste images à laquelle on ajoute des images
        self.images.append(pygame.transform.scale(pygame.image.load("Radio0.png").convert_alpha(),
                                                  (TUILE_TAILLE, TUILE_TAILLE)))  # image 1 de la liste
        self.images.append(pygame.transform.scale(pygame.image.load("Radio1.png").convert_alpha(),
                                                  (TUILE_TAILLE, TUILE_TAILLE)))  # image 2 de la liste
        self.images.append(pygame.transform.scale(pygame.image.load("Radio2.png").convert_alpha(),
                                                  (TUILE_TAILLE, TUILE_TAILLE)))  # image 3 de la liste
        self.images.append(pygame.transform.scale(pygame.image.load("Radio3.png").convert_alpha(),
                                                  (TUILE_TAILLE, TUILE_TAILLE)))  # image 4 de la liste
        self.id_frame = 0  # on initialise la frame à 0
        self.image = self.images[self.id_frame]  # l'image de base est la frame 0
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        # Voici l'attribut
        self.ACTION = False
        # On initialise la liste d'actions
        self.actions = []
        # On initialise le compteur d'action
        self.id_action = 0
        # On ajoute le texte des 4 actions
        self.actions.append("Le président de la République a annoncé ce soir le confinement pour lutter contre le "
                            "SARS-COV-2")
        self.actions.append("Nous ne savons pas encore combien de temps ce confinement national durera...")
        self.actions.append("Restez bien chez-vous et respectez les gestes barrières...")
        self.actions.append("Et bon courage pour vous occuper !")
        # On initialise l'action actuelle
        self.action_actuelle = self.actions[self.id_action]
        self.collision = True
        self.anime = True
        self.action = True
        self.inventaire = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)

    # On crée un fonction update pour modifier les frames à chaque update
    def update(self):
        self.id_frame += 0.1  # on incrémente la frame de 0.1 (pour que les frames n'aillent pas trop vite,
        # on est sur 30 fps)
        if math.floor(self.id_frame) >= len(self.images):
            self.id_frame = 0  # si la partie entière de l'id de la frame est plus grand que le total de frames on
            # repart à 0
        self.image = self.images[math.floor(self.id_frame)]  # on actualise l'image à chaque update

        # Quand la radio est activée
        if self.ACTION:
            # Tant que le compteur est inférieur au nombre d'actions
            while self.id_action < len(self.actions):
                # on actualise l'action actuelle
                self.action_actuelle = self.actions[self.id_action]
                # on incrémente le compteur d'action
                self.id_action += 1
                # On affiche le message correspondant à l'action et on attend la réponse du joueur
                fenetre.fill(NOIR)
                texte = police2.render(self.action, False, BLANC)
                fenetre.blit(texte, (25, 25))
                pygame.display.update()
                attente_message()
            # On remet le compteur à zéro et on désactive l'action
            self.id_action = 0
            self.ACTION = False


# Les portes sont actionnables
class PORTE(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.images = []  # on fait une liste images à laquelle on ajoute des images
        self.images.append(
            pygame.transform.scale(pygame.image.load("Porte.png").convert_alpha(), (30, 30)))  # Image porte fermée
        self.images.append(
            pygame.transform.scale(pygame.image.load("Sol.png").convert_alpha(), (30, 30)))  # Image porte ouverte
        self.image = self.images[0]  # l'image de base est la frame 0
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.ACTION = False
        # On définit si la porte est ouverte ou non
        self.ouvert = False
        # On définit si la porte est la porte de sortie
        self.sortie = False
        # La porte est la porte de sortie si elle se trouve a la tuile 14
        if y == 14:
            self.sortie = True
        self.collision = True
        self.anime = False
        self.action = True
        self.inventaire = False

        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)

    def update(self):
        # Si la porte est actionnée
        if self.ACTION:
            # Si ce n'est pas la porte de sortie
            if not self.sortie:
                # Si la porte était ouverte
                if self.ouvert:
                    # L'image devient celle de la porte
                    self.image = self.images[0]
                    # On rajoute le sprite de la porte à la liste des murs
                    LISTE_MURS.add(self)
                    # On met à jour l'attribut ouvert
                    self.ouvert = False
                else:
                    # L'image devient celle du sol
                    self.image = self.images[1]
                    # On enlève le sprite de la liste des murs
                    LISTE_MURS.remove(self)
                    # On met à jour l'attribut ouvert
                    self.ouvert = True
                # On arrete l'action
                self.ACTION = False
            else:
                # Si c'est la porte de sortie et que tous les mini jeux ne sont pas finis
                if MINIJEUX != [True, True, True]:
                    #On affiche le message
                    fenetre.fill(NOIR)
                    texte = police2.render("Je ne crois pas que ça soit une bonne idée de sortir...", False, BLANC)
                    fenetre.blit(texte, (25, 25))
                    pygame.display.update()
                    attente_message()
                    #On met à jour ACTION
                    self.ACTION = False
                else:
                    #Si c'est la porte de sortie on affiche le message de réussite
                    fenetre.fill(NOIR)
                    mixer.music.stop()
                    sound_winner()
                    texte = police2.render("J'ai bien mérité d'aller me promener une petite heure...", False, BLANC)
                    fenetre.blit(texte, (25, 25))
                    pygame.display.update()
                    attente_message()
                    self.ACTION = False
                    LISTE_MURS.empty()
                    LISTE_SPRITES.empty()
                    LISTE_SPRITES_ANIMES.empty()
                    LISTE_SPRITES_ACTION.empty()
                    LISTE_SPRITES_PARAM.empty()
                    LISTE_SPRITES_INVENTAIRE.empty()
                    LISTE_DECHETS.empty()
                    LISTE_SPRITES_SELECT_INV.empty()
                    LISTE_SPRITES_SHOOTER.empty()
                    LISTE_SPRITES_ANIMES_SHOOTER.empty()
                    LISTE_SPRITES_MONSTRE.empty()
                    LISTE_SPRITES_PERSO.empty()
                    from Game import menu
                    menu()


class EVIER(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.images = []  # on fait une liste images à laquelle on ajoute des images
        self.images.append(pygame.image.load("Evier0.png").convert_alpha())  # image 1 de la liste
        self.images.append(pygame.image.load("Evier1.png").convert_alpha())  # image 2 de la liste
        self.images.append(pygame.image.load("Evier2.png").convert_alpha())  # image 3 de la liste
        self.images.append(pygame.image.load("Evier3.png").convert_alpha())  # image 4 de la liste
        self.id_frame = 0  # on initialise la frame à 0
        self.image = self.images[self.id_frame]  # l'image de base est la frame 0
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.ACTION = False
        #On ajoute un attribut ANIME pour savoir si on anime la fuite de l'évier ou pas
        self.ANIME = True
        self.collision = True
        self.anime = False
        self.action = True
        self.inventaire = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.inventaire == True:
            LISTE_SPRITES_INVENTAIRE.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)

    def update(self):
        #Si l'evier est animé on incrémente le compteur d'image.
        if self.ANIME:
            self.id_frame += 0.2
            if math.floor(self.id_frame) >= len(self.images):
                self.id_frame = 1
            self.image = self.images[math.floor(self.id_frame)]
            #Si l'évier est actionné on lance le mini jeu de réparation
            if self.ACTION:
                Jeu_evier.evier_reparation()

                self.ANIME = False
        #Sinon l'image reste celle de base
        else:
            self.id_frame = 0
            self.image = self.images[self.id_frame]




#La cheminée est juste animée
class CHEMINEE(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.images = []  # on fait une liste images à laquelle on ajoute des images
        self.images.append(pygame.image.load("Cheminee0.png").convert_alpha())  # image 1 de la liste
        self.images.append(pygame.image.load("Cheminee1.png").convert_alpha())  # image 2 de la liste
        self.images.append(pygame.image.load("Cheminee2.png").convert_alpha())  # image 3 de la liste
        self.images.append(pygame.image.load("Cheminee3.png").convert_alpha())  # image 4 de la liste
        self.images.append(pygame.image.load("Cheminee4.png").convert_alpha())  # image 5 de la liste
        self.images.append(pygame.image.load("Cheminee5.png").convert_alpha())  # image 6 de la liste
        self.id_frame = 0  # on initialise la frame à 0
        self.image = self.images[self.id_frame]  # l'image de base est la frame 0
        self.rect = self.image.get_rect()
        self.rect.y = TUILE_TAILLE * y + SCORE_HAUTEUR
        self.rect.x = TUILE_TAILLE * x
        self.collision = True
        self.anime = True
        self.action = False
        if self.collision == True:
            LISTE_MURS.add(self)
        if self.anime == True:
            LISTE_SPRITES_ANIMES.add(self)
        if self.action == True:
            LISTE_SPRITES_ACTION.add(self)

    def update(self):
        self.id_frame += 0.2  # on incrémente la frame de 0.1 (pour que les frames n'aillent pas trop vite,
        # on est sur 30 fps)
        if math.floor(self.id_frame) >= len(self.images):
            self.id_frame = 0  # si la partie entière de l'id de la frame est plus grand que le total de frames on
            # repart à 0
        self.image = self.images[math.floor(self.id_frame)]  # on actualise l'image à chaque update

#On crée une fonction pour laisser le temps au joueur de lire les messages
def attente_message():
    #On initialise la variable d'arrêt de la boucle
    ATTENTE = True
    #On génère et affiche le message
    message = police2.render("Appuyez sur une touche pour continuer", False, BLANC)
    fenetre.blit(message, (LARGEUR / 2 - INVENTAIRE_LARGEUR, HAUTEUR - TUILE_TAILLE))
    pygame.display.update()
    while ATTENTE:
        #On attend que le joueur appuie sur une touche avant d'interrompre la boucle
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                ATTENTE = False
